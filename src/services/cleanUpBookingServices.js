const { Query } = require("mongoose");
const connect = require("../db/connect");

async function cleanUpBooking(){
    const currentDate = new Date();
    currentDate.setDate(currentDate.getDate()-7)//Definir a data de threshold para 7 dias atras
    
    const formattedDate = currentDate.toISOString().split ('T')[0];//Formata a data para YYYY-MM-DD
    const query = `DELETE FROM schedule WHERE dateEnd < ?`
    const values =[formattedDate];

    return new Promise((resolve, reject)=>{
        connect.query(query,values,function(err,results){
            if(err){
                console.error("Erro MYSQL",error);
                return reject(new Error("Erro ao limpar agendamentos"));
            }
            console.log("Agendamentos antigos apagados");
            resolve("Agendamentos antigos limpos com sucesso");
        });
    });
}
module.exports = cleanUpBooking; 