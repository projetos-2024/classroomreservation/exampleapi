const connect = require("../db/connect");

module.exports = class bookingController {
  static async createBooking(req, res) {
    //Criação da const com os atributos passados na tabela schedule
    const { dateStart, dateEnd, days, user, classroom, timeStart, timeEnd } = req.body;

    if (
      !dateStart ||
      !dateEnd ||
      !days ||
      !user ||
      !classroom ||
      !timeStart ||
      !timeEnd
    ) {
      return res
        .status(400)
        .json({ error: "Todos os campos devem ser preenchidos" });
    }

    //Aqui será feitas validações especificas desse banco de dados
    //Transformação dos dias que está em um array para uma string.
    const dayString = days.join(",");

    //Verificando caso não haja uma reserva já existente
    //Procurar valores na dayString no banco de dados
    try {
      const overlapQuery = `SELECT * FROM schedule
        WHERE classroom = '${classroom}'
         AND (
            (dateStart<='${dateEnd}' AND dateEnd >='${dateStart}')
        )AND (
            (timeStart<='${timeEnd}' AND timeEnd >='${timeStart}')
        )AND (
            (days LIKE '%Seg%' AND '${dayString}' LIKE '%Seg%')OR
            (days LIKE '%Ter%' AND '${dayString}' LIKE '%Ter%')OR
            (days LIKE '%Qua%' AND '${dayString}' LIKE '%Qua%')OR
            (days LIKE '%Qui%' AND '${dayString}' LIKE '%Qui%')OR
            (days LIKE '%Sex%' AND '${dayString}' LIKE '%Sex%')OR
            (days LIKE '%Sab%' AND '${dayString}' LIKE '%Sab%')
        )
        `;

      connect.query(overlapQuery, function (err, results) {
        if (err) {
          return res
            .status(500)
            .json({ error: "Erro ao verificar agendamento" });
        }
        //Se houver resultado a consulta, ja existe agendamento
        if (results.length > 0) {
          return res
            .status(400)
            .json({ error: "Sala ocupada para mesmos dias e/ou horários" });
        }

        //Caso a query nao retorne nada inserimos na tabela
        const insertQuery = `INSERT INTO schedule( 
            dateStart,
            dateEnd, 
            days,
            user, 
            classroom,
            timeStart, 
            timeEnd) 
            VALUES (
                '${dateStart}',
                '${dateEnd}',
                '${dayString}',
                '${user}',
                '${classroom}',
                '${timeStart}',
                '${timeEnd}'
            )
        `;

        //Executando a query de inserção
        connect.query(insertQuery, function (err) {
          if (err) {
            console.error(err);
            return res
              .status(500)
              .json({ error: "Erro ao cadastrar agendamento" });
          }
          return res
            .status(201)
            .json({ message: "Agendamento cadastrado com sucesso" });
        });
      });
    } catch (error) {
      console.error("Erro ao executar a consulta:", error);
      res.status(500).json({ error: "Erro interno de servidor " });
    }
  }
};
