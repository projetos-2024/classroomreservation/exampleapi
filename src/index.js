const express = require('express')
const cors = require('cors');
const testConnect = require("./db/testeConnect");
const cron = require("./cron");//Importa o agendador cron 

class AppController {
    constructor() {
      this.express = express();
      this.middlewares();
      this.routes();
      testConnect();
      
    }

    middlewares() {
      this.express.use(express.json());
      this.express.use(cors());
    }

    routes() {
      const apiRoutes= require('./routes/apiRoutes');
      this.express.use('/api/reservas/v1/', apiRoutes);
      this.express.get('/status', (req, res) => {
        res.status(200).json({ message: 'API do Santucci' });
      });
    }


  }

  module.exports = new AppController().express;