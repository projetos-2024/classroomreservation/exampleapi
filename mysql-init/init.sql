CREATE DATABASE IF NOT EXISTS classroombooking;

USE classroombooking;

CREATE TABLE IF NOT EXISTS `user` (
  `cpf` VARCHAR(11) PRIMARY KEY,  -- Usando CPF como chave primária, já que é único
  `name` VARCHAR(100) NOT NULL,
  `email` VARCHAR(100) NOT NULL,
  `password` VARCHAR(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Inserção de dados de exemplo na tabela `user`
INSERT INTO `user` (`cpf`, `name`, `email`, `password`) VALUES
('12345678901', 'Isadora', 'isadora@gmail.com', '1234'),
('98765432109', 'Euller', 'euller@gmail.com', '1234'),
('45678912301', 'Adriano', 'adriano@gmail.com', '1234');

-- Estrutura da tabela `classroom`
CREATE TABLE IF NOT EXISTS `classroom` (
  `number` INT PRIMARY KEY,  -- Número da sala como chave primária
  `description` VARCHAR(255) NOT NULL,
  `capacity` INT NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Inserção de dados de exemplo na tabela `classroom`
INSERT INTO `classroom` (`number`, `description`, `capacity`) VALUES
(101, 'Sala de conferências', 50),
(102, 'Laboratório de informática', 30),
(103, 'Sala de reuniões', 20);

-- Estrutura da tabela `schedule`
CREATE TABLE IF NOT EXISTS `schedule` (
  `id` INT AUTO_INCREMENT PRIMARY KEY,
  `dateStart` DATE NOT NULL,
  `dateEnd` DATE NOT NULL,
  `days` VARCHAR(255) NOT NULL,  -- Dias de reserva (ex: 'Seg, Ter, Qua')
  `user_cpf` VARCHAR(11) NOT NULL,  -- CPF do usuário que reservou
  `classroom` INT NOT NULL,  -- Número da sala
  `timeStart` TIME NOT NULL,
  `timeEnd` TIME NOT NULL,
  FOREIGN KEY (`user_cpf`) REFERENCES `user`(`cpf`) ON DELETE CASCADE,
  FOREIGN KEY (`classroom`) REFERENCES `classroom`(`number`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Inserção de dados de exemplo na tabela `schedule`
INSERT INTO `schedule` (`dateStart`, `dateEnd`, `days`, `user_cpf`, `classroom`, `timeStart`, `timeEnd`) VALUES
('2024-10-10', '2024-10-12', 'Seg, Ter', '12345678901', 101, '09:00', '11:00'),
('2024-10-13', '2024-10-15', 'Qua, Qui', '98765432109', 102, '14:00', '16:00');


COMMIT;