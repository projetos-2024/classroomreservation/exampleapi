const router = require("express").Router();
const userController = require("../controller/userController");
const classroomController = require("../controller/classroomController");
const bookingController = require("../controller/bookingController");

//User
router.post("/user/", userController.createUser);
router.post("/user/login", userController.postLogin);
router.get("/user/", userController.getAllUsers);
router.get("/user/:id", userController.getUserById);
router.put("/user/:id", userController.updateUser);
router.delete("/user/:id", userController.deleteUser);

//Classroom
router.post("/classroom/", classroomController.createClassroom);
router.get("/classroom/", classroomController.getAllClassrooms);
router.get("/classroom/:number", classroomController.getClassroomById);


//Booking
router.post("/schedule/", bookingController.createBooking);


module.exports = router;
